import { Row, Col, Form } from "react-bootstrap";
import { FormWrapper } from "./FormWrapper";
import { AccountFormProps } from "./types/types";

export function AccountForm({ email, password, updateFields, }: AccountFormProps) {
  return (
    <FormWrapper title="Account Creation">
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>Email</Form.Label>
          <Form.Control autoFocus required type="email" value={email} onChange={e => updateFields({ email: e.target.value })} placeholder="Email" />
        </Col>
      </Row>
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>Password</Form.Label>
          <Form.Control required type="password" value={password} onChange={e => updateFields({ password: e.target.value })} placeholder="Password" />
        </Col>
      </Row>
    </FormWrapper>
  )
}