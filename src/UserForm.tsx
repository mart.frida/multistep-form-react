import { Row, Col, Form, Button } from "react-bootstrap";
import { FormWrapper } from "./FormWrapper";
import { UserFormProps } from "./types/types";

export function UserForm({ firstName, lastName, age, updateFields }: UserFormProps) {
  return (
    <FormWrapper title="User Details">
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>First name</Form.Label>
          <Form.Control type="text" value={firstName} onChange={e => updateFields({ firstName: e.target.value })} placeholder="Enter name" />
        </Col>
      </Row>
      <Row className="m-2 justify-content-md-center">
        <Col>
          <Form.Label>Last name</Form.Label>
          <Form.Control type="text" value={lastName} onChange={e => updateFields({ lastName: e.target.value })} placeholder="Enter last name" />
        </Col>
      </Row>
      <Row className="m-2 justify-content-md-center">
        <Col order-md-last>
          <Form.Label>Age</Form.Label>
          <Form.Control required min={1} type="number" value={age} onChange={e => updateFields({ age: e.target.value })} placeholder="Enter age" />
          <Form.Text className="text-muted">
            We'll never share your data with anyone else.
          </Form.Text>
        </Col>
      </Row >
    </FormWrapper>
  )
}