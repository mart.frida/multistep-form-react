import { ReactNode } from "react"
import { FormWrapperProps } from "./types/types"
import Container from "react-bootstrap/esm/Container"

export function FormWrapper({ title, children }: FormWrapperProps) {
  return <>
    <Container>
      <h2 className="text-center">{title}</h2>
      <div>{children}</div>
    </Container>
  </>
}