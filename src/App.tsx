import { FormEvent, useState } from "react"
import { AccountForm } from "./AccountForm"
import { AdressForm } from "./AdressForm"
import { useMultistepForm } from "./useMultistepForm"
import { UserForm } from "./UserForm"
import { FormData } from './types/types'
import { Button } from "react-bootstrap"

import 'bootstrap/dist/css/bootstrap.min.css';


const INITIAL_DATA: FormData = {
  firstName: '',
  lastName: '',
  age: '',
  street: '',
  city: '',
  state: '',
  zip: '',
  email: '',
  password: '',
}

function App() {
  const [data, setData] = useState(INITIAL_DATA)
  function updateFields(fields: Partial<FormData>) {
    setData(prev => {
      return { ...prev, ...fields }
    })
  }

  const { steps, currentStepIndex, step, isFirstStep, back, next, isLastStep } = useMultistepForm([
    <UserForm {...data} updateFields={updateFields} />,
    <AdressForm {...data} updateFields={updateFields} />,
    <AccountForm {...data} updateFields={updateFields} />])

  function onSubmit(e: FormEvent) {
    e.preventDefault()
    if (!isLastStep) return next()
    alert("Successful Account Creation")
  }

  return (
    <>
      <div className="col-md-5 my-5 mx-auto p-3 shadow-lg rounded-3 border position-relative">
        <form onSubmit={onSubmit} >
          <div className="float-end">
            {currentStepIndex + 1}/{steps.length}
          </div>
          {step}
          <div className="d-flex justify-content-end ">
            {!isFirstStep && (
              <Button onClick={back} className="btn btn-md px-5 mx-4 mt-2" variant="secondary" type="submit">
                Back
              </Button>
            )}
            <Button className="btn btn-md px-5 mx-1 mt-2" variant="secondary" type="submit">
              {isLastStep ? "Finish" : "Next"}
            </Button>
          </div>
        </form>
      </div >



      {/* эффект удаления */}
      {/* <div className="form-floating">
          <input className="form-control" type="email" placeholder="Enter name"></input>
          <Form.Label>First name</Form.Label>
        </div> */}



    </>

  )
}

export default App
