import { Row, Col, Form } from "react-bootstrap";
import { FormWrapper } from "./FormWrapper";
import { AddressFormProps } from "./types/types";

export function AdressForm({ street, city, state, zip, updateFields, }: AddressFormProps) {
  return (
    <FormWrapper title="Address">
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>Street</Form.Label>
          <Form.Control autoFocus required type="text" value={street} onChange={e => updateFields({ street: e.target.value })} placeholder="Street" />
        </Col>
      </Row>
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>City</Form.Label>
          <Form.Control required type="text" value={city} onChange={e => updateFields({ city: e.target.value })} placeholder="City" />
        </Col>
      </Row>
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>State</Form.Label>
          <Form.Control required type="text" value={state} onChange={e => updateFields({ state: e.target.value })} placeholder="State" />
        </Col>
      </Row>
      <Row className=" m-2 justify-content-md-center">
        <Col >
          <Form.Label>Zip</Form.Label>
          <Form.Control required type="text" value={zip} onChange={e => updateFields({ zip: e.target.value })} placeholder="Zip" />
        </Col>
      </Row>
    </FormWrapper>
  )
}